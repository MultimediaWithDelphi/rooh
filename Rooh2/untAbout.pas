unit untAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, jpeg, ExtCtrls, Buttons, OleCtrls, SHDocVw,
  untMain;

type
  TfrmAbout = class(TForm)
    ActionList1: TActionList;
    actClose: TAction;
    WebBrowser1: TWebBrowser;
    imgAboutBack: TImage;
    imgBack: TImage;
    imgDistribute: TImage;
    imgProduct: TImage;
    imgIntroduction: TImage;
    lblIntroduction: TLabel;
    lblProduct: TLabel;
    lblDistribute: TLabel;
    lblBack: TLabel;
    Timer1: TTimer;
    procedure actCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lblIntroductionMouseEnter(Sender: TObject);
    procedure lblIntroductionMouseLeave(Sender: TObject);
    procedure lblProductMouseEnter(Sender: TObject);
    procedure lblProductMouseLeave(Sender: TObject);
    procedure lblDistributeMouseEnter(Sender: TObject);
    procedure lblDistributeMouseLeave(Sender: TObject);
    procedure lblBackMouseEnter(Sender: TObject);
    procedure lblBackMouseLeave(Sender: TObject);
    procedure lblIntroductionClick(Sender: TObject);
    procedure lblProductClick(Sender: TObject);
    procedure lblDistributeClick(Sender: TObject);
    procedure lblBackClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    procedure Imaging(n: Byte);
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;
  ImgNo: Byte;

implementation

{$R *.dfm}

procedure TfrmAbout.actCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmAbout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmAbout.lblIntroductionMouseEnter(Sender: TObject);
begin
  imgIntroduction.Visible := True;
end;

procedure TfrmAbout.lblIntroductionMouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfrmAbout.lblProductMouseEnter(Sender: TObject);
begin
  imgProduct.Visible := True;
end;

procedure TfrmAbout.lblProductMouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfrmAbout.lblDistributeMouseEnter(Sender: TObject);
begin
  imgDistribute.Visible := True;
end;

procedure TfrmAbout.lblDistributeMouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfrmAbout.lblBackMouseEnter(Sender: TObject);
begin
  imgBack.Visible := True;
end;

procedure TfrmAbout.lblBackMouseLeave(Sender: TObject);
begin
  imgBack.Visible := False;
end;

procedure TfrmAbout.lblIntroductionClick(Sender: TObject);
begin
  WebBrowser1.Navigate(MyDir('Data\A1.htm'));
  Imaging(1);
end;

procedure TfrmAbout.lblProductClick(Sender: TObject);
begin
  WebBrowser1.Navigate(MyDir('Data\A2.htm'));
  Imaging(2);
end;

procedure TfrmAbout.lblDistributeClick(Sender: TObject);
begin
  WebBrowser1.Navigate(MyDir('Data\A3.htm'));
  Imaging(3);
end;

procedure TfrmAbout.lblBackClick(Sender: TObject);
begin
  actClose.OnExecute(Sender);
end;

procedure TfrmAbout.Imaging(n: Byte);
begin
  if n <> 0 then
    ImgNo := n
  else
    n := ImgNo;
  imgIntroduction.Visible := False;
  imgProduct.Visible := False;
  imgDistribute.Visible := False;
  case n of
    1: begin imgIntroduction.Visible := True; end;
    2: begin imgProduct.Visible := True; end;
    3: begin imgDistribute.Visible := True; end;
  end;
end;

procedure TfrmAbout.FormShow(Sender: TObject);
begin
  WebBrowser1.Navigate(MyDir('Data\A1.htm'));
  Imaging(1);
  Timer1.Enabled := True;
end;

procedure TfrmAbout.Timer1Timer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := AlphaBlendValue + 50;
    if ab > 255 then
    begin
      ab := 255;
      AlphaBlendValue := ab;
      AlphaBlend := False;
      Timer1.Enabled := False;
    end
    else
      AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := AlphaBlendValue - 50;
    if ab < 0 then
    begin
      ab := 0;
      AlphaBlendValue := ab;
      Timer1.Enabled := False;
      Close;
    end
    else
      AlphaBlendValue := ab;
  end
end;

procedure TfrmAbout.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Tag = 0 then
  begin
    CanClose := False;
    Tag := 1;
    AlphaBlend := True;
    Timer1.Enabled := True;
  end;
end;

end.
