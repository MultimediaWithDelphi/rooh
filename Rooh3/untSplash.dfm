object frmSplash: TfrmSplash
  Left = 158
  Top = 93
  BorderStyle = bsNone
  ClientHeight = 600
  ClientWidth = 800
  Color = 4269568
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object swf1: TShockwaveFlash
    Left = 104
    Top = 72
    Width = 192
    Height = 192
    TabOrder = 0
    ControlData = {
      6755665500030000D8130000D813000008000200000000000800020000000000
      080002000000000008000E000000570069006E0064006F00770000000B00FFFF
      0B00FFFF08000A0000004800690067006800000008000200000000000B00FFFF
      080002000000000008000E00000061006C007700610079007300000008001000
      0000530068006F00770041006C006C0000000B0000000B000000080002000000
      00000800020000000000}
  end
  object ActionList1: TActionList
    Left = 336
    Top = 16
    object actCloseSplash: TAction
      Caption = 'CloseSplash'
      SecondaryShortCuts.Strings = (
        'ESC'
        'SPACE')
      OnExecute = actCloseSplashExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    Left = 264
    Top = 16
  end
  object ppmSplash: TPopupMenu
    Left = 192
    Top = 16
  end
end
