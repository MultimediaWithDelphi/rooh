unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MPlayer, Buttons, ComCtrls, VolCtrl, jpeg,
  ActnList, ShellAPI, Menus, OleCtrls, ImgList;

const
  MaxNo = 8;
  WM_ICONTRAY = WM_USER + 1;
  DATA_FOLDER = 'data\';
  VIDEO_FOLDER = 'video\';
  AUDIO_FOLDER = 'audio\';

type
  TAudioVideo = (avAudio, avVideo);
  TfrmMain = class(TForm)
    Timer1: TTimer;
    lbl1: TLabel;
    lbl4: TLabel;
    imgMain: TImage;
    img1: TImage;
    img2: TImage;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    img3: TImage;
    img4: TImage;
    img5: TImage;
    img6: TImage;
    img7: TImage;
    img8: TImage;
    imgAbout: TImage;
    lblAbout: TLabel;
    imgMin: TImage;
    lblMin: TLabel;
    imgExit: TImage;
    lblExit: TLabel;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    mplVideo: TMediaPlayer;
    tmrBlend: TTimer;
    mplBack: TMediaPlayer;
    imgMusic: TImage;
    lblMusic: TLabel;
    pnlScreen: TPanel;
    pnlMediaPlayer: TPanel;
    imgStop: TImage;
    imgRewind: TImage;
    imgRepeat: TImage;
    imgPre: TImage;
    imgPlay: TImage;
    imgPauseO: TImage;
    imgPause: TImage;
    imgNext: TImage;
    imgLoop: TImage;
    imgForward: TImage;
    lblTime: TLabel;
    lblStop: TLabel;
    lblRewind: TLabel;
    lblRepeat: TLabel;
    lblPrevious: TLabel;
    lblPlayPause: TLabel;
    lblNext: TLabel;
    lblLoop: TLabel;
    lblFullTime: TLabel;
    lblForward: TLabel;
    imgTrackMove: TImage;
    lblTrackUser: TLabel;
    imgPreD: TImage;
    imgRwD: TImage;
    imgStopD: TImage;
    imgFwD: TImage;
    imgNextD: TImage;
    imgRepD: TImage;
    imgLoopD: TImage;
    imgPauseD: TImage;
    imgPlayD: TImage;
    imgSound: TImage;
    lblChangeSound: TLabel;
    lblSysTray: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    imgSysTray: TImage;
    actFullScreen: TAction;
    imgMute: TImage;
    imgMuteD: TImage;
    lblMute: TLabel;
    ImageList1: TImageList;
    mVolControl1: TmVolControl;
    imgPlayer: TImage;
    imgVideo: TImage;
    imgAudio: TImage;
    lblAudio: TLabel;
    lblVideo: TLabel;
    mplAudio: TMediaPlayer;
    imgDisplayPanel: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblLoopClick(Sender: TObject);
    procedure lblRepeatClick(Sender: TObject);
    procedure lbl1Click(Sender: TObject);
    procedure lbl4Click(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblRepeatMouseEnter(Sender: TObject);
    procedure lblRepeatMouseLeave(Sender: TObject);
    procedure lblLoopMouseEnter(Sender: TObject);
    procedure lblLoopMouseLeave(Sender: TObject);
    procedure lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbl1MouseEnter(Sender: TObject);
    procedure lbl1MouseLeave(Sender: TObject);
    procedure PlayFile(FileNo: Byte);
    procedure Imaging(No: Byte);
    procedure lbl4MouseEnter(Sender: TObject);
    procedure lblForwardClick(Sender: TObject);
    procedure lblRewindClick(Sender: TObject);
    procedure lblNextClick(Sender: TObject);
    procedure lblPreviousClick(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure lblPreviousMouseEnter(Sender: TObject);
    procedure lblPreviousMouseLeave(Sender: TObject);
    procedure lblRewindMouseEnter(Sender: TObject);
    procedure lblRewindMouseLeave(Sender: TObject);
    procedure lblForwardMouseEnter(Sender: TObject);
    procedure lblForwardMouseLeave(Sender: TObject);
    procedure lblNextMouseEnter(Sender: TObject);
    procedure lblNextMouseLeave(Sender: TObject);
    procedure lbl2MouseEnter(Sender: TObject);
    procedure lbl3MouseEnter(Sender: TObject);
    procedure lbl3Click(Sender: TObject);
    procedure lbl5MouseEnter(Sender: TObject);
    procedure lbl6MouseEnter(Sender: TObject);
    procedure lbl7MouseEnter(Sender: TObject);
    procedure lblAboutMouseEnter(Sender: TObject);
    procedure lblAboutMouseLeave(Sender: TObject);
    procedure lblMinMouseEnter(Sender: TObject);
    procedure lblMinMouseLeave(Sender: TObject);
    procedure lblMinClick(Sender: TObject);
    procedure lblExitMouseEnter(Sender: TObject);
    procedure lblExitMouseLeave(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure lbl2Click(Sender: TObject);
    procedure lbl5Click(Sender: TObject);
    procedure lbl6Click(Sender: TObject);
    procedure lbl7Click(Sender: TObject);
    procedure lbl8Click(Sender: TObject);
    procedure lblAboutClick(Sender: TObject);
    procedure lblChangeSoundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tmrBlendTimer(Sender: TObject);
    procedure mplBackNotify(Sender: TObject);
    procedure lblMusicMouseEnter(Sender: TObject);
    procedure lblMusicMouseLeave(Sender: TObject);
    procedure lblMusicClick(Sender: TObject);
    procedure lbl8MouseEnter(Sender: TObject);
    procedure lblPreviousMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPreviousMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblSysTrayClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure lblSysTrayMouseEnter(Sender: TObject);
    procedure lblSysTrayMouseLeave(Sender: TObject);
    procedure actFullScreenExecute(Sender: TObject);
    procedure pnlScreenDblClick(Sender: TObject);
    procedure actMuteExecute(Sender: TObject);
    procedure lblMuteClick(Sender: TObject);
    procedure lblMuteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseEnter(Sender: TObject);
    procedure lblMuteMouseLeave(Sender: TObject);
    procedure lblAudioMouseEnter(Sender: TObject);
    procedure lblAudioMouseLeave(Sender: TObject);
    procedure lblVideoMouseEnter(Sender: TObject);
    procedure lblVideoMouseLeave(Sender: TObject);
    procedure lblVideoClick(Sender: TObject);
    procedure lblAudioClick(Sender: TObject);
    procedure mplVideoNotify(Sender: TObject);
    procedure mplAudioNotify(Sender: TObject);
  private
    TrayIconData: TNotifyIconData;
    procedure InitialTrayIcon;
    procedure TrayMessage(var Msg: TMessage); message WM_ICONTRAY;
    function MiliSecondToTime(Sec: Longint): AnsiString;
    procedure UpdateCurrTime;
    function UpdateFileName(n: Byte): String;
    procedure SoundHeightShow;
    procedure ChangeSound(v: Smallint);
    procedure Imaging1;
    procedure SwitchAV(AV: TAudioVideo);//(No: Integer);
    procedure MediaPlayer1Notify(Sender: TObject);
//    function GetWinDir: TFileName;
    { Private declarations }
  public
    { Public declarations }
  end;

procedure SetMute;
procedure ResetMute;
function MyDir(S: String): String;

var
  frmMain: TfrmMain;
  NowPlay: Byte;  // 0=none
  Vol: Byte;
  Mute: Boolean;
  Rep, Loop: Boolean;
  VideoPath, AudioPath, DataPath: String;
  mplBackPlay: Boolean;
  AudioVideo: TAudioVideo;
  MediaPlayer1: TMediaPlayer;

implementation

uses Math, untAbout, untSplash, untBackground, untFullScreen, Types,
  untSplashLogo;

{$R *.dfm}

function MyDir(S: String): String;
begin
  S := GetCurrentDir + '\' + S;
  while Pos('\\', S) > 0 do
    Delete(S, Pos('\\', S), 1);
  Result := S;
end;

procedure TfrmMain.InitialTrayIcon;
begin
  PopUpMenu1.OwnerDraw:=True;

  with TrayIconData do
  begin
    cbSize := SizeOf(TrayIconData);
    Wnd := Handle;
    uID := 0;
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    uCallbackMessage := WM_ICONTRAY;
    hIcon := Application.Icon.Handle;
    StrPCopy(szTip, Application.Title);
  end;

  Shell_NotifyIcon(NIM_ADD, @TrayIconData);
end;

procedure TfrmMain.TrayMessage(var Msg: TMessage);
var
  p : TPoint;
begin
{  case Msg.lParam of
    WM_LBUTTONDOWN:
    begin
      ShowMessage('This icon responds to RIGHT BUTTON click!');
    end;
    WM_RBUTTONDOWN:
    begin
       SetForegroundWindow(Handle);
       GetCursorPos(p);
       PopUpMenu1.Popup(p.x, p.y);
       PostMessage(Handle, WM_NULL, 0, 0);
    end;
  end;}
  if (Msg.LParam = WM_LBUTTONDOWN) or (Msg.LParam = WM_RBUTTONDOWN) then
  begin
    SetForegroundWindow(Handle);
    GetCursorPos(p);
    PopUpMenu1.Popup(p.x, p.y);
    PostMessage(Handle, WM_NULL, 0, 0);
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  frmSplashLogo := TfrmSplashLogo.Create(Self);
  frmSplashLogo.ShowModal;

  frmBackground := TfrmBackground.Create(Self);
  frmBackground.Show;
  frmBackground.Enabled := False;

  VideoPath := MyDir(VIDEO_FOLDER);
  AudioPath := MyDir(AUDIO_FOLDER);
  DataPath := MyDir(DATA_FOLDER);

  swfMovie := DataPath + 'Sys02.dll';
  frmSplash := TfrmSplash.Create(Self);
  frmSplash.ShowModal;

  Rep := False;
  Loop := False;
  NowPlay := 0; // No file is playing.
  SwitchAV(avVideo);
  Vol := mVolControl1.Volume;
  Mute := False;
  if Mute then
    imgMute.Visible := True
  else
    imgMute.Visible := False;
  SoundHeightShow;
  try
    mplBack.FileName := DataPath + 'b.mp3';
    mplBack.Open;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
  except;end;
end;

procedure SetMute;
begin
  Mute := True;
end;

procedure ResetMute;
begin
  Mute := False;
end;

(*function TForm1.GetWinDir: TFileName;
var
  SysFolder: String;
begin
  SetLength(SysFolder, 255);
  SetLength(SysFolder, GetWindowsDirectory(
    PChar(SysFolder), Length(SysFolder)));
  Result := SysFolder;
end;*)

procedure TfrmMain.UpdateCurrTime;
var
  cp, l, t: Longint; // Current Position, Length
begin
  try
    cp := MediaPlayer1.Position;// div 1000;
    l := MediaPlayer1.Length;// div 1000;
    t := cp * lblTrackUser.Width div l;
   // if not (t >= 316) then  // 317 is the length of lblTrackbar
     // t := (t div 5) * 5;
    imgTrackMove.Width := t;
//    imgTrackMove.Width := lblTrackUser.Width - t;
//    imgTrackMove.Left := lblTrackUser.Left + t;
    lblTime.Caption := MiliSecondToTime(cp);
  except
    ;
  end;
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
  try
    UpdateCurrTime;
    if MediaPlayer1.Position
      >= MediaPlayer1.Length then
    begin
      if Rep then
      begin
        MediaPlayer1.Rewind;
        MediaPlayer1.Play;
        MediaPlayer1.Notify := True;
      end
      else
      begin
        if Loop then
        begin
     //     if NowPlay in [1..MaxFileNo] then
     //     begin
            if NowPlay + 1 > MaxNo then
            begin
              PlayFile(1)
            end
            else
              PlayFile(NowPlay+1);
      //    end;
        end
        else
          lblStopClick(Sender);
      end;
    end;
  except
    ;
  end;
end;

procedure TfrmMain.MediaPlayer1Notify(Sender: TObject);
begin
      try
        if (mplBack.Mode = mpPlaying) and (MediaPlayer1.Mode = mpPlaying) then
        begin
          mplBack.Stop;
          mplBackPlay := False;
        end;
        mplBack.Notify := True;
      except;end;
  with Sender as TMediaPlayer do
  begin
    Notify := True;
    if (Mode = mpPaused) or (Mode = mpStopped) then
    begin
      imgPause.Visible := False;
      imgPauseO.Visible := False;
    end
    else if Mode = mpPlaying then
    begin
      imgPlay.Visible := False;
      imgPause.Visible := True;
    end;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  tmrBlend.Enabled := True;
  MediaPlayer1.Notify := True;
end;

function TfrmMain.UpdateFileName(n: Byte): String;
var
  fn: String;
begin
  if AudioVideo = avAudio then
    fn := AudioPath + IntToStr(n) + '.mp3'
  else if AudioVideo = avVideo then
    fn := VideoPath + IntToStr(n) + '.wmv';
  if fn <> MediaPlayer1.FileName then
  begin
   // imgPlayerTitle.Picture.LoadFromFile(DataPath + IntToStr(n) + '.jpg');
    try
      MediaPlayer1.Close;
    except
      ;
    end;
    imgTrackMove.Width := 0;
    try
      MediaPlayer1.FileName := fn;
      MediaPlayer1.Open;
      lblPlayPause.Enabled := True;
      lblFullTime.Caption := MiliSecondToTime(MediaPlayer1.Length);
      NowPlay := n;
      Imaging1;
    except;end;
//    Imaging(NowPlay);
  end;
end;

procedure TfrmMain.PlayFile(FileNo: Byte);
var
  t: TRect;
begin
 // imgPlayerWithScreen.Visible := True;
  lblTrackUser.Visible := True;
  pnlMediaPlayer.Visible := True;
  try
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
    mplBack.Notify := True;
  except;end;
  UpdateFileName(FileNo);
  //pnlScreen.Visible := True;
  try
    MediaPlayer1.Play;
    MediaPlayer1Notify(MediaPlayer1);
    Timer1.Enabled := True;
  except; end;
  if AudioVideo = avVideo then
  begin
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := MediaPlayer1.Display.Width;
      Bottom := MediaPlayer1.Display.Height;
    end;
    MediaPlayer1.DisplayRect := t;
  end;
end;

function TfrmMain.MiliSecondToTime(Sec: Longint): AnsiString;
var
  h, m, s: Byte;
  TimeStr: AnsiString;
begin
  Sec := Sec div 1000;
  m := Sec div 60;
  s := Sec - (m * 60);
  h := 0;
  if m >= 60 then
  begin
    h := m div 60;
    m := m - (h * 60);
  end;
  TimeStr := '';
  if h <> 0 then
    if m < 10 then
      TimeStr := IntToStr(h) + ':0'
    else
      TimeStr := IntToStr(h) + ':';
  TimeStr := TimeStr + IntToStr(m) + ':';
  if s < 10 then
    TimeStr := TimeStr + '0';
  TimeStr := TimeStr + IntToStr(s);
  Result := TimeStr;
end;

procedure TfrmMain.Imaging1;//(No: Integer);
begin
  img1.Visible := False;
  img2.Visible := False;
  img3.Visible := False;
  img4.Visible := False;
  img5.Visible := False;
  img6.Visible := False;
  img7.Visible := False;
  img8.Visible := False;
  case NowPlay of
    1: img1.Visible := True;
    2: img2.Visible := True;
    3: img3.Visible := True;
    4: img4.Visible := True;
    5: img5.Visible := True;
    6: img6.Visible := True;
    7: img7.Visible := True;
    8: img8.Visible := True;
  end;
end;

procedure TfrmMain.Imaging(No: Byte);
begin
  if No <> 0 then
    PlayFile(No);
  Imaging1;//(No);
end;

procedure TfrmMain.lblLoopClick(Sender: TObject);
begin
  Loop := not Loop;
  if Loop then
  begin
    Rep := False;
    imgRepeat.Visible := False;
  end;
end;

procedure TfrmMain.lblRepeatClick(Sender: TObject);
begin
  Rep := not Rep;
  if Rep then
  begin
    Loop := False;
    imgLoop.Visible := False;
  end;
end;

procedure TfrmMain.SoundHeightShow;
begin
  imgSound.Width := (Vol * lblChangeSound.Width) div 255;
end;

procedure TfrmMain.lbl1Click(Sender: TObject);
begin
  Imaging(1);
end;

procedure TfrmMain.lbl4Click(Sender: TObject);
begin
  Imaging(4);
end;

procedure TfrmMain.lblStopClick(Sender: TObject);
begin
  MediaPlayer1.Notify := True;
  try
//    imgTrackMove.Width := lblTrackUser.Width;
  //  imgTrackMove.Left := lblTrackUser.Left;
    Timer1.Enabled := False;
    MediaPlayer1.Rewind;
    MediaPlayer1.Stop;
    imgTrackMove.Width := 0;
  except
    ;
  end;
end;

procedure TfrmMain.lblPlayPauseClick(Sender: TObject);
begin
  try
    MediaPlayer1.Notify := True;
    if NowPlay <> 0 then
    begin
      if imgPause.Visible then
        MediaPlayer1.Stop
      else
      begin
        Timer1.Enabled := True;
        MediaPlayer1.Play;
        MediaPlayer1Notify(MediaPlayer1);
      end;
    end;
  except;end;
end;

procedure TfrmMain.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseO.Visible := True
    else if not imgPause.Visible then
      imgPlay.Visible := True;
end;

procedure TfrmMain.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPauseO.Visible then
    imgPauseO.Visible := False;
  if imgPlay.Visible then
    imgPlay.Visible := False;
end;

procedure TfrmMain.lblStopMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    imgStop.Visible := True;
end;

procedure TfrmMain.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible := False;
end;

procedure TfrmMain.lblRepeatMouseEnter(Sender: TObject);
begin
  imgRepeat.Visible := True;
end;

procedure TfrmMain.lblRepeatMouseLeave(Sender: TObject);
begin
  if not Rep then
    imgRepeat.Visible := False;
end;

procedure TfrmMain.lblLoopMouseEnter(Sender: TObject);
begin
  imgLoop.Visible := True;
end;

procedure TfrmMain.lblLoopMouseLeave(Sender: TObject);
begin
  if not Loop then
    imgLoop.Visible := False;
end;

procedure TfrmMain.lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := MediaPlayer1.Length;// div 1000;
//    t := (lblTrackUser.Width - X) * l
  //    div lblTrackUser.Width;
    t := X * l div lblTrackUser.Width;
    if not Timer1.Enabled then
    begin
//      imgTrackMove.Width := lblTrackUser.Width - X;
//      imgTrackMove.Left := lblTrackUser.Left + X;
      imgTrackMove.Width := X;
    end;
    MediaPlayer1.Position := t;
    MediaPlayer1.Notify := True;
    if imgPause.Visible then
    begin
      MediaPlayer1.Play;
      MediaPlayer1.Notify := True;
    end;
  except
    ;
  end;
end;

procedure TfrmMain.lbl1MouseEnter(Sender: TObject);
begin
  img1.Visible := True;
end;

procedure TfrmMain.lbl1MouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfrmMain.lbl4MouseEnter(Sender: TObject);
begin
  img4.Visible := True;
end;

procedure TfrmMain.lblForwardClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position + 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMain.lblRewindClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position - 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMain.lblNextClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay + 1;
      if NowPlay > MaxNo then
      begin
        NowPlay := 1;
      end;
      if (MediaPlayer1.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    MediaPlayer1.Notify := True;
  except;end;
end;

procedure TfrmMain.lblPreviousClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay - 1;
      if NowPlay < 1 then
      begin
        NowPlay := MaxNo;
      end;
      if (MediaPlayer1.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    MediaPlayer1.Notify := True;
  except;end;
end;

procedure TfrmMain.actVolHighExecute(Sender: TObject);
begin
  ChangeSound(Vol + 26);
end;

procedure TfrmMain.actVolLowExecute(Sender: TObject);
begin
  ChangeSound(Vol - 26);
end;

procedure TfrmMain.lblPreviousMouseEnter(Sender: TObject);
begin
  imgPre.Visible := True;
end;

procedure TfrmMain.lblPreviousMouseLeave(Sender: TObject);
begin
  imgPre.Visible := False;
end;

procedure TfrmMain.lblRewindMouseEnter(Sender: TObject);
begin
  imgRewind.Visible := True;
end;

procedure TfrmMain.lblRewindMouseLeave(Sender: TObject);
begin
  imgRewind.Visible := False;
end;

procedure TfrmMain.lblForwardMouseEnter(Sender: TObject);
begin
  imgForward.Visible := True;
end;

procedure TfrmMain.lblForwardMouseLeave(Sender: TObject);
begin
  imgForward.Visible := False;
end;

procedure TfrmMain.lblNextMouseEnter(Sender: TObject);
begin
  imgNext.Visible := True;
end;

procedure TfrmMain.lblNextMouseLeave(Sender: TObject);
begin
  imgNext.Visible := False;
end;

procedure TfrmMain.lbl2MouseEnter(Sender: TObject);
begin
  img2.Visible := True;
end;

procedure TfrmMain.lbl3MouseEnter(Sender: TObject);
begin
  img3.Visible := True;
end;

procedure TfrmMain.lbl3Click(Sender: TObject);
begin
  Imaging(3);
end;

procedure TfrmMain.lbl5MouseEnter(Sender: TObject);
begin
  img5.Visible := True;
end;

procedure TfrmMain.lbl6MouseEnter(Sender: TObject);
begin
  img6.Visible := True;
end;

procedure TfrmMain.lbl7MouseEnter(Sender: TObject);
begin
  img7.Visible := True;
end;

procedure TfrmMain.lblAboutMouseEnter(Sender: TObject);
begin
  imgAbout.Visible := True;
end;

procedure TfrmMain.lblAboutMouseLeave(Sender: TObject);
begin
  imgAbout.Visible := False;
end;

procedure TfrmMain.lblMinMouseEnter(Sender: TObject);
begin
  imgMin.Visible := True;
end;

procedure TfrmMain.lblMinMouseLeave(Sender: TObject);
begin
  imgMin.Visible := False;
end;

procedure TfrmMain.lblMinClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmMain.lblExitMouseEnter(Sender: TObject);
begin
  imgExit.Visible := True;
end;

procedure TfrmMain.lblExitMouseLeave(Sender: TObject);
begin
  imgExit.Visible := False;
end;

procedure TfrmMain.lblExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.lbl2Click(Sender: TObject);
begin
  Imaging(2);
end;

procedure TfrmMain.lbl5Click(Sender: TObject);
begin
  Imaging(5);
end;

procedure TfrmMain.lbl6Click(Sender: TObject);
begin
  Imaging(6);
end;

procedure TfrmMain.lbl7Click(Sender: TObject);
begin
  Imaging(7);
end;

procedure TfrmMain.lbl8Click(Sender: TObject);
begin
  Imaging(8);
end;

procedure TfrmMain.lblAboutClick(Sender: TObject);
begin
  frmAbout := TfrmAbout.Create(Self);
  frmAbout.ShowModal;
end;

procedure TfrmMain.ChangeSound(v: Smallint);
begin
  if v < 0 then
    v := 0
  else if v > 255 then
    v := 255;
  Vol := v;
  SoundHeightShow;
  if not Mute then
    mVolControl1.Volume := Vol;
end;

procedure TfrmMain.lblChangeSoundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := 255;
    t := X * l div lblChangeSound.Width;
    ChangeSound(t);
  except
    ;
  end;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  try
    mplBack.Close;
    MediaPlayer1.Close;
    Timer1.Enabled := False;
  except;end;
  if Tag = 0 then
  begin
    CanClose := False;
    frmMain.AlphaBlend := True;
    Tag := 1;
    tmrBlend.Enabled := True;
  end;
end;

procedure TfrmMain.tmrBlendTimer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := frmMain.AlphaBlendValue + 50;
    if ab > 255 then
    begin
      ab := 255;
      frmMain.AlphaBlendValue := ab;
      frmMain.AlphaBlend := False;
      tmrBlend.Enabled := False;
    end
    else
      frmMain.AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := frmMain.AlphaBlendValue - 50;
    if ab < 0 then
    begin
      ab := 0;
      frmMain.AlphaBlendValue := ab;
      tmrBlend.Enabled := False;
      swfMovie := DataPath + 'Sys03.dll';
      frmSplash := TfrmSplash.Create(Self);
      //frmSplash.Timer1.Interval := 1000;//47000;
      frmSplash.ShowModal;
      Close;
    end
    else
      frmMain.AlphaBlendValue := ab;
  end
end;

procedure TfrmMain.mplBackNotify(Sender: TObject);
begin
  try
    if (mplBack.Mode = mpStopped) and (mplBackPlay = True) then
    begin
      mplBack.Play;
      mplBackPlay := True;
    end
    else if mplBack.Position >= mplBack.Length then
    begin
      mplBack.Rewind;
      mplBack.Play;
    end;
  except;end;
  mplBack.Notify := True;
end;

procedure TfrmMain.lblMusicMouseEnter(Sender: TObject);
begin
  imgMusic.Visible := True;
end;

procedure TfrmMain.lblMusicMouseLeave(Sender: TObject);
begin
  imgMusic.Visible := False;
end;

procedure TfrmMain.lblMusicClick(Sender: TObject);
begin
//  Imaging(0);
  try
    MediaPlayer1.Notify := False;
    if MediaPlayer1.Mode = mpPlaying then
      MediaPlayer1.Stop;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
    MediaPlayer1.Notify := True;
    imgPause.Visible := False;
    imgPauseD.Visible := False;
    imgPauseO.Visible := False;
  except;
  end;
end;

procedure TfrmMain.lbl8MouseEnter(Sender: TObject);
begin
  img8.Visible := True;
end;

procedure TfrmMain.lblPreviousMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := True;
end;

procedure TfrmMain.lblPreviousMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := False;
end;

procedure TfrmMain.lblRewindMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := True;
end;

procedure TfrmMain.lblRewindMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := False;
end;

procedure TfrmMain.lblStopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := True;
end;

procedure TfrmMain.lblStopMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := False;
end;

procedure TfrmMain.lblForwardMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := True;
end;

procedure TfrmMain.lblForwardMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := False;
end;

procedure TfrmMain.lblNextMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := True;
end;

procedure TfrmMain.lblNextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := False;
end;

procedure TfrmMain.lblRepeatMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := True;
end;

procedure TfrmMain.lblRepeatMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := False;
end;

procedure TfrmMain.lblLoopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := True;
end;

procedure TfrmMain.lblLoopMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := False;
end;

procedure TfrmMain.lblPlayPauseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseD.Visible := True
    else if not imgPause.Visible then
      imgPlayD.Visible := True;
end;

procedure TfrmMain.lblPlayPauseMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
//  if imgPauseO.Visible then
    imgPauseD.Visible := False;
//  if imgPlay.Visible then
    imgPlayD.Visible := False;
end;

procedure TfrmMain.lblSysTrayClick(Sender: TObject);
begin
  InitialTrayIcon;
  try
    if MediaPlayer1.Mode = mpPlaying then
      MediaPlayer1.Stop;
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
  except;end;
  Hide;
  frmBackground.Hide;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMain.N1Click(Sender: TObject);
begin
  frmBackground.Show;
  frmMain.Show;
  if mplBackPlay then
    mplBack.Play;
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMain.N2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.N8Click(Sender: TObject);
begin
  if (mplBack.Mode = mpPlaying) then
  begin
    mplBack.Stop;
    mplBackPlay := False;
  end;
end;

procedure TfrmMain.lblSysTrayMouseEnter(Sender: TObject);
begin
  imgSysTray.Visible := True;
end;

procedure TfrmMain.lblSysTrayMouseLeave(Sender: TObject);
begin
  imgSysTray.Visible := False;
end;

procedure TfrmMain.actFullScreenExecute(Sender: TObject);
var
  t: TRect;
begin
  try
    frmFullScreen := TfrmFullScreen.Create(Self);
    MediaPlayer1.Display := frmFullScreen.Panel1;
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := frmFullScreen.Panel1.Width;
      Bottom := frmFullScreen.Panel1.Height;
    end;
    MediaPlayer1.DisplayRect := t;
//    MediaPlayer1.Display.
    frmFullScreen.ShowModal;
  except;end;
end;

procedure TfrmMain.pnlScreenDblClick(Sender: TObject);
begin
  if AudioVideo = avVideo then
    actFullScreen.OnExecute(Sender);
end;

procedure TfrmMain.actMuteExecute(Sender: TObject);
begin
  if not Mute then
  begin
    mVolControl1.Volume := 0;
    imgMute.Visible := True;
    SetMute;
  end
  else
  begin
    mVolControl1.Volume := Vol;
    SoundHeightShow;
    imgMute.Visible := False;
    ResetMute;
  end;
end;

procedure TfrmMain.lblMuteClick(Sender: TObject);
begin
  actMute.OnExecute(Sender);
end;

procedure TfrmMain.lblMuteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := True;
end;

procedure TfrmMain.lblMuteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := False;
end;

procedure TfrmMain.lblMuteMouseEnter(Sender: TObject);
begin
  imgMute.Visible := True;
end;

procedure TfrmMain.lblMuteMouseLeave(Sender: TObject);
begin
  if not Mute then
    imgMute.Visible := False;
end;

procedure TfrmMain.lblAudioMouseEnter(Sender: TObject);
begin
  imgAudio.Visible := True;
end;

procedure TfrmMain.lblAudioMouseLeave(Sender: TObject);
begin
  if not (AudioVideo = avAudio) then
    imgAudio.Visible := False;
end;

procedure TfrmMain.lblVideoMouseEnter(Sender: TObject);
begin
  imgVideo.Visible := True;
end;

procedure TfrmMain.lblVideoMouseLeave(Sender: TObject);
begin
  if not (AudioVideo = avVideo) then
    imgVideo.Visible := False;
end;

procedure TfrmMain.SwitchAV(AV: TAudioVideo);
begin
  if not (AudioVideo = AV) then
  begin
    AudioVideo := AV;
    if AV = avAudio then
    begin
      imgVideo.Visible := False;
      imgAudio.Visible := True;
      try
        //mplVideo.Close;
        MediaPlayer1.Close;
        Timer1.Enabled := False;
        MediaPlayer1Notify(MediaPlayer1);
      except; end;
      MediaPlayer1 := mplAudio;
    end else if AV = avVideo then
    begin
      imgVideo.Visible := True;
      imgAudio.Visible := False;
      try
        //mplAudio.Close;
        MediaPlayer1.Close;
        Timer1.Enabled := False;
        MediaPlayer1Notify(MediaPlayer1);
      except; end;
      MediaPlayer1 := mplAudio;
      MediaPlayer1.Display := pnlScreen;
    end;
    PlayFile(NowPlay);
  end;
end;

procedure TfrmMain.lblVideoClick(Sender: TObject);
begin
  SwitchAV(avVideo);
end;

procedure TfrmMain.lblAudioClick(Sender: TObject);
begin
  SwitchAV(avAudio);
end;

procedure TfrmMain.mplVideoNotify(Sender: TObject);
begin
  MediaPlayer1Notify(Sender);
end;

procedure TfrmMain.mplAudioNotify(Sender: TObject);
begin
  MediaPlayer1Notify(Sender);
end;

end.
