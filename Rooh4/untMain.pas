unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, MPlayer, ExtCtrls, StdCtrls, jpeg, MyVolControl, Menus, ImgList,
  ShellAPI;

const
  MaxNo = 4;
  WM_ICONTRAY = WM_USER + 1;
  DATA_FOLDER = 'data\';
  VIDEO_FOLDER = 'video\';
  AUDIO_FOLDER = 'audio\';

type
  TfrmMain = class(TForm)
    mplVideo: TMediaPlayer;
    mplBack: TMediaPlayer;
    pnlScreen: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lblPre: TLabel;
    lblRew: TLabel;
    lblPlayPause: TLabel;
    lblPause: TLabel;
    lblFor: TLabel;
    lblNext: TLabel;
    lblStop: TLabel;
    lblTime: TLabel;
    lblFullTime: TLabel;
    lblTrackMove: TLabel;
    lblTrackbar: TLabel;
    lblRep: TLabel;
    lblLoop: TLabel;
    Timer1: TTimer;
    lblPLay: TLabel;
    imgMain: TImage;
    lblMusic: TLabel;
    lblChangeSound: TLabel;
    lblSound1: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    ImageList1: TImageList;
    procedure Label1MouseEnter(Sender: TObject);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label2MouseEnter(Sender: TObject);
    procedure Label2MouseLeave(Sender: TObject);
    procedure Label3MouseEnter(Sender: TObject);
    procedure Label3MouseLeave(Sender: TObject);
    procedure Label4MouseEnter(Sender: TObject);
    procedure Label4MouseLeave(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lblPreMouseEnter(Sender: TObject);
    procedure lblPreMouseLeave(Sender: TObject);
    procedure lblRewMouseEnter(Sender: TObject);
    procedure lblRewMouseLeave(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblPauseMouseEnter(Sender: TObject);
    procedure lblPauseMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblForMouseEnter(Sender: TObject);
    procedure lblForMouseLeave(Sender: TObject);
    procedure lblNextMouseEnter(Sender: TObject);
    procedure lblNextMouseLeave(Sender: TObject);
    procedure lblRepMouseEnter(Sender: TObject);
    procedure lblRepMouseLeave(Sender: TObject);
    procedure lblLoopMouseEnter(Sender: TObject);
    procedure lblLoopMouseLeave(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure mplVideoNotify(Sender: TObject);
    procedure lblPLayMouseEnter(Sender: TObject);
    procedure lblPLayMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblLoopClick(Sender: TObject);
    procedure lblRepClick(Sender: TObject);
    procedure lblTrackbarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMusicClick(Sender: TObject);
    procedure lblChangeSoundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    TrayIconData: TNotifyIconData;
    procedure UpdateCurrTime;
    function MiliSecondToTime(Sec: Integer): AnsiString;
    procedure PlayFile(FileNo: Byte);
    function UpdateFileName(n: Byte): String;
    procedure Imaging1;
    procedure Imaging(No: Byte);
    procedure ChangeSound(v: Smallint);
    procedure SoundHeightShow;
    procedure InitialTrayIcon;
    procedure TrayMessage(var Msg: TMessage);
    { Private declarations }
  public
    { Public declarations }
  end;

procedure SetMute;
procedure ResetMute;
function MyDir(S: String): String;

var
  frmMain: TfrmMain;
  NowPlay: Byte;  // 0=none
  Vol: Smallint;
  Mute: Boolean;
  Rep, Loop: Boolean;
  VideoPath, AudioPath, DataPath: String;
  mplBackPlay: Boolean;

implementation

uses untBackground, untSplash;

{$R *.dfm}

procedure SetMute;
begin
  Mute := True;
end;

procedure ResetMute;
begin
  Mute := False;
end;

function MyDir(S: String): String;
begin
  S := GetCurrentDir + '\' + S;
  while Pos('\\', S) > 0 do
    Delete(S, Pos('\\', S), 1);
  Result := S;
end;

procedure TfrmMain.InitialTrayIcon;
begin
  PopUpMenu1.OwnerDraw:=True;

  with TrayIconData do
  begin
    cbSize := SizeOf(TrayIconData);
    Wnd := Handle;
    uID := 0;
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    uCallbackMessage := WM_ICONTRAY;
    hIcon := Application.Icon.Handle;
    StrPCopy(szTip, Application.Title);
  end;

  Shell_NotifyIcon(NIM_ADD, @TrayIconData);
end;

procedure TfrmMain.TrayMessage(var Msg: TMessage);
var
  p : TPoint;
begin
{  case Msg.lParam of
    WM_LBUTTONDOWN:
    begin
      ShowMessage('This icon responds to RIGHT BUTTON click!');
    end;
    WM_RBUTTONDOWN:
    begin
       SetForegroundWindow(Handle);
       GetCursorPos(p);
       PopUpMenu1.Popup(p.x, p.y);
       PostMessage(Handle, WM_NULL, 0, 0);
    end;
  end;}
  if (Msg.LParam = WM_LBUTTONDOWN) or (Msg.LParam = WM_RBUTTONDOWN) then
  begin
    SetForegroundWindow(Handle);
    GetCursorPos(p);
    PopUpMenu1.Popup(p.x, p.y);
    PostMessage(Handle, WM_NULL, 0, 0);
  end;
end;

procedure TfrmMain.Label1MouseEnter(Sender: TObject);
begin
  Label1.Transparent := False;
end;

procedure TfrmMain.Label1MouseLeave(Sender: TObject);
begin
  Imaging1;
end;

procedure TfrmMain.Label2MouseEnter(Sender: TObject);
begin
  Label2.Transparent := False;
end;

procedure TfrmMain.Label2MouseLeave(Sender: TObject);
begin
  Label2.Transparent := True;
end;

procedure TfrmMain.Label3MouseEnter(Sender: TObject);
begin
  Label3.Transparent := False;
end;

procedure TfrmMain.Label3MouseLeave(Sender: TObject);
begin
  Label3.Transparent := True;
end;

procedure TfrmMain.Label4MouseEnter(Sender: TObject);
begin
  Label4.Transparent := False;
end;

procedure TfrmMain.Label4MouseLeave(Sender: TObject);
begin
  Label4.Transparent := True;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Screen.Cursors[1] := LoadCursor(0, IDC_HAND);

  frmBackground := TfrmBackground.Create(Self);
  frmBackground.Show;
  frmBackground.Enabled := False;

  WinExec(PChar('regsvr32.exe /s' + DataPath + 'Sys01.ocx'), SW_NORMAL);
  swfMovie := DataPath + 'Sys02.dll';
  frmSplash := TfrmSplash.Create(Self);
  frmSplash.ShowModal;

  VideoPath := MyDir(VIDEO_FOLDER);
  AudioPath := MyDir(AUDIO_FOLDER);
  DataPath := MyDir(DATA_FOLDER);
  Rep := False;
  Loop := False;
  NowPlay := 0; // No file is playing.
  Vol := MPGetVolume(mplVideo);
  Mute := False;
  if Mute then
    //imgMute.Visible := True
  else
   // imgMute.Visible := False;
  SoundHeightShow;
  try
    mplBack.FileName := DataPath + 'b.mp3';
    mplBack.Open;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
  except;end;
end;

procedure TfrmMain.lblPreMouseEnter(Sender: TObject);
begin
  lblPre.Transparent := False;
end;

procedure TfrmMain.lblPreMouseLeave(Sender: TObject);
begin
  lblPre.Transparent := True;
end;

procedure TfrmMain.lblRewMouseEnter(Sender: TObject);
begin
  lblRew.Transparent := False;
end;

procedure TfrmMain.lblRewMouseLeave(Sender: TObject);
begin
  lblRew.Transparent := True;
end;

procedure TfrmMain.lblPlayPauseMouseEnter(Sender: TObject);
begin
  lblPlayPause.Transparent := False;
end;

procedure TfrmMain.lblPlayPauseMouseLeave(Sender: TObject);
begin
  lblPlayPause.Transparent := True;
end;

procedure TfrmMain.lblPauseMouseEnter(Sender: TObject);
begin
  lblPause.Transparent := False;
end;

procedure TfrmMain.lblPauseMouseLeave(Sender: TObject);
begin
  lblPause.Transparent := True;
end;

procedure TfrmMain.lblStopMouseEnter(Sender: TObject);
begin
  lblStop.Transparent := False;
end;

procedure TfrmMain.lblStopMouseLeave(Sender: TObject);
begin
  lblStop.Transparent := True;
end;

procedure TfrmMain.lblForMouseEnter(Sender: TObject);
begin
  lblFor.Transparent := False;
end;

procedure TfrmMain.lblForMouseLeave(Sender: TObject);
begin
  lblFor.Transparent := True;
end;

procedure TfrmMain.lblNextMouseEnter(Sender: TObject);
begin
  lblNext.Transparent := False;
end;

procedure TfrmMain.lblNextMouseLeave(Sender: TObject);
begin
  lblNext.Transparent := True;
end;

procedure TfrmMain.lblRepMouseEnter(Sender: TObject);
begin
  lblRep.Transparent := False;
end;

procedure TfrmMain.lblRepMouseLeave(Sender: TObject);
begin
  if not Rep then
    lblRep.Transparent := True;
end;

procedure TfrmMain.lblLoopMouseEnter(Sender: TObject);
begin
  lblLoop.Transparent := False;
end;

procedure TfrmMain.lblLoopMouseLeave(Sender: TObject);
begin
  if not Loop then
    lblLoop.Transparent := True;
end;

procedure TfrmMain.Label1Click(Sender: TObject);
begin
  Imaging(1);
end;

procedure TfrmMain.Label2Click(Sender: TObject);
begin
  Imaging(2);
end;

procedure TfrmMain.Label3Click(Sender: TObject);
begin
  Imaging(3);
end;

procedure TfrmMain.Label4Click(Sender: TObject);
begin
  Imaging(4);
end;

procedure TfrmMain.UpdateCurrTime;
var
  cp, l, t: Int64; // Current Position, Length
begin
  try
    cp := mplVideo.Position;
    l := mplVideo.Length;
    t := cp * lblTrackbar.Width div l;
    lblTrackMove.Width := t;
    lblTime.Caption := MiliSecondToTime(cp);
  except
    ;
  end;
end;

function TfrmMain.MiliSecondToTime(Sec: Longint): AnsiString;
var
  h, m, s: Byte;
  TimeStr: AnsiString;
begin
  Sec := Sec div 1000;
  m := Sec div 60;
  s := Sec - (m * 60);
  h := 0;
  if m >= 60 then
  begin
    h := m div 60;
    m := m - (h * 60);
  end;
  TimeStr := '';
  if h <> 0 then
    if m < 10 then
      TimeStr := IntToStr(h) + ':0'
    else
      TimeStr := IntToStr(h) + ':';
  TimeStr := TimeStr + IntToStr(m) + ':';
  if s < 10 then
    TimeStr := TimeStr + '0';
  TimeStr := TimeStr + IntToStr(s);
  Result := TimeStr;
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
  try
    UpdateCurrTime;
    if mplVideo.Position
      >= mplVideo.Length then
    begin
      if Rep then
      begin
        with mplVideo do
        begin
          Rewind;
          Play;
          Notify := True;
        end;
      end
      else
      begin
        if Loop then
        begin
            if NowPlay + 1 > MaxNo then
            begin
              PlayFile(1)
            end
            else
              PlayFile(NowPlay+1);
        end
        else
          lblStopClick(Sender);
      end;
    end;
  except
    ;
  end;
end;

procedure TfrmMain.PlayFile(FileNo: Byte);
var
  t: TRect;
begin
  try
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
    mplBack.Notify := True;
  except;end;
  UpdateFileName(FileNo);
  try
    mplVideo.Play;
    mplVideoNotify(mplVideo);
    Timer1.Enabled := True;
  except; end;
//  if AudioVideo = avVideo then
  begin
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := mplVideo.Display.Width;
      Bottom := mplVideo.Display.Height;
    end;
    mplVideo.DisplayRect := t;
  end;
end;

procedure TfrmMain.lblStopClick(Sender: TObject);
begin
  mplVideo.Notify := True;
  try
//    imgTrackMove.Width := lblTrackUser.Width;
  //  imgTrackMove.Left := lblTrackUser.Left;
    Timer1.Enabled := False;
    mplVideo.Rewind;
    mplVideo.Stop;
    lblTrackMove.Width := 0;
  except
    ;
  end;
end;

function TfrmMain.UpdateFileName(n: Byte): String;
var
  fn: String;
begin
//  if AudioVideo = avAudio then
  //  fn := AudioPath + IntToStr(n) + '.mp3'
//  else if AudioVideo = avVideo then
    fn := VideoPath + IntToStr(n) + '.wmv';
  if fn <> mplVideo.FileName then
  begin
    try
      mplVideo.Close;
    except
      ;
    end;
    lblTrackMove.Width := 0;
    try
      mplVideo.FileName := fn;
      mplVideo.Open;
      lblPlayPause.Enabled := True;
      lblFullTime.Caption := MiliSecondToTime(mplVideo.Length);
      NowPlay := n;
      Imaging1;
    except;end;
//    Imaging(NowPlay);
  end;
end;

procedure TfrmMain.mplVideoNotify(Sender: TObject);
begin
      try
        if (mplBack.Mode = mpPlaying) and (mplVideo.Mode = mpPlaying) then
        begin
          mplBack.Stop;
          mplBackPlay := False;
        end;
        mplBack.Notify := True;
      except;end;
  with Sender as TMediaPlayer do
  begin
    Notify := True;
    if (Mode = mpPaused) or (Mode = mpStopped) then
    begin
      lblPause.Visible := False;
      //imgPauseO.Visible := False;
    end
    else if Mode = mpPlaying then
    begin
      lblPlay.Visible := False;
      lblPause.Visible := True;
    end;
  end;
end;

procedure TfrmMain.Imaging1;//(No: Integer);
begin
  Label1.Transparent := True;
  Label2.Transparent := True;
  Label3.Transparent := True;
  Label4.Transparent := True;
  case NowPlay of
    1: Label1.Transparent := False;
    2: Label2.Transparent := False;
    3: Label3.Transparent := False;
    4: Label4.Transparent := False;
  end;
end;

procedure TfrmMain.lblPLayMouseEnter(Sender: TObject);
begin
  lblPLay.Transparent := False;
end;

procedure TfrmMain.lblPLayMouseLeave(Sender: TObject);
begin
  lblPLay.Transparent := True;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  mplVideo.Notify := True;
end;

procedure TfrmMain.lblLoopClick(Sender: TObject);
begin
  Loop := not Loop;
  if Loop then
  begin
    Rep := False;
    lblRep.Transparent := True;
  end;
end;

procedure TfrmMain.lblRepClick(Sender: TObject);
begin
  Rep := not Rep;
  if Rep then
  begin
    Loop := False;
    lblLoop.Transparent := True;
  end;
end;

procedure TfrmMain.lblTrackbarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := mplVideo.Length;// div 1000;
    t := X * l div lblTrackbar.Width;
    if not Timer1.Enabled then
    begin
      lblTrackMove.Width := X;
    end;
    mplVideo.Position := t;
    mplVideo.Notify := True;
    if lblPause.Visible then
    begin
      mplVideo.Play;
      mplVideo.Notify := True;
    end;
  except
    ;
  end;
end;

procedure TfrmMain.Imaging(No: Byte);
begin
  if No <> 0 then
    PlayFile(No);
  Imaging1;//(No);
end;

procedure TfrmMain.lblMusicClick(Sender: TObject);
begin
  try
    mplVideo.Notify := False;
    if mplVideo.Mode = mpPlaying then
      mplVideo.Stop;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
    mplVideo.Notify := True;
    lblPause.Transparent := True;
    //imgPauseD.Visible := False;
    //imgPauseO.Visible := False;
  except;
  end;
end;

procedure TfrmMain.lblChangeSoundMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := 1000;
    t := X * l div lblChangeSound.Width;
    ChangeSound(t);
  except
    ;
  end;
end;

procedure TfrmMain.ChangeSound(v: Smallint);
begin
  if v < 0 then
    v := 0
  else if v > 1000 then
    v := 1000;
  Vol := v;
  SoundHeightShow;
  if not Mute then
    MPSetVolume(mplVideo, Vol);
end;

procedure TfrmMain.SoundHeightShow;
begin
  lblSound1.Width := (Vol * lblChangeSound.Width) div 1000;
end;

end.
